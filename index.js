/******************************BÀI 1***************/
// hàm click
function tinhKetQua() {
  //gọi hàm khu vực
  var khuVucValue = document.getElementById("txt-khu-vuc").value;
  var getKhuVuc = diemKhuVuc(khuVucValue);

  // gọi hàm đối tượng
  var getDoiTuong;
  var doiTuongValue = document.getElementById("txt-doi-tuong").value * 1;
  getDoiTuong = diemDoiTuong(doiTuongValue);

  // gọi hàm tính điểm
  var score1Value = document.getElementById("txt-diem1").value * 1;
  var score2Value = document.getElementById("txt-diem2").value * 1;
  var score3Value = document.getElementById("txt-diem3").value * 1;
  var tongDiem = tinhDiem(
    score1Value,
    score2Value,
    score3Value,
    getKhuVuc,
    getDoiTuong
  );

  // điểm chuẩn
  var diemChuanValue = document.getElementById("txt-diem-chuan").value * 1;

  //proccess
  var thongBaoKq = "";
  if (diemChuanValue >= 1 && diemChuanValue <= 35) {
    if (tongDiem >= diemChuanValue) {
      thongBaoKq = ` <p>Bạn đã trúng tuyển</p>
                    <p>Tổng điểm của bạn: ${tongDiem.toFixed(1)}</p> `;
    } else {
      thongBaoKq = ` <p>Bạn không trúng tuyển</p>
                    <p>Tổng điểm của bạn: ${tongDiem.toFixed(1)}</p> `;
    }
  } else {
    thongBaoKq = "Input không hợp lệ";
  }
  document.getElementById("result").innerHTML = thongBaoKq;
}
// hàm khu vực
function diemKhuVuc(khuVuc) {
  switch (khuVuc) {
    case "A": {
      return 2;
    }
    case "B": {
      return 1;
    }
    case "C": {
      return 0.5;
    }
    case "X": {
      return 0;
    }
  }
}
// hàm đối tượng
function diemDoiTuong(doiTuong) {
  switch (doiTuong) {
    case 1: {
      return 2.5;
    }
    case 2: {
      return 1.5;
    }
    case 3: {
      return 1;
    }
    case 0: {
      return 0;
    }
  }
}
//hàm tính điểm
function tinhDiem(diem1, diem2, diem3, diemKhuVuc, diemDoiTuong) {
  var tinhDiem = diem1 + diem2 + diem3 + diemKhuVuc + diemDoiTuong;
  return tinhDiem;
}

/******************************BÀI 2***************/
/*
Đề bài: viết ct tính tiền điện cho nhập tên và số kW
  giá tiền điện:
    50kw đầu: 500 
    50kw kế (từ 51 - 100) :650 
    100kw kế (101 - 200) :850
    150kw kế (201 - 350): 1100
    còn lại (lớn hơn 350):1300

  yêu cầu: tính tiền tiền điện theo giá này
  ----------------------------------------------------------------------
  giờ mình làm gì?
    tiền điện = giá tiền điện * sokw
    giá tiền điện: cho ct pt trước
    số sokw điện: lấy từ input
  làm gì đầu tiên?
    truyền giá tiền điện cho từng kW
    dùng tác hàm - tham số truyền vào lúc gọi hàm input nhập dô
*/

// hàm click
function tinTienDien() {
  var nameValue = document.getElementById("txt-ten").value;
  var sokWValue = document.getElementById("txt-soKW").value * 1;
  var giaTienDienValue = giaTienDiem(sokWValue);
  var tongTienValue = 0;
  if (sokWValue <= 50) {
    tongTienValue = tinhTien(sokWValue, giaTienDienValue);
  } else if (sokWValue > 50 && sokWValue <= 100) {
    tongTienValue = tinhTien(sokWValue, giaTienDienValue);
  } else if (sokWValue > 100 && sokWValue <= 200) {
    tongTienValue = tinhTien(sokWValue, giaTienDienValue);
  } else if (sokWValue > 200 && sokWValue <= 350) {
    tongTienValue = tinhTien(sokWValue, giaTienDienValue);
  } else {
    tongTienValue = tinhTien(sokWValue, giaTienDienValue);
  }
  document.getElementById("thongBao").innerHTML =
    "Tổng tiền của" +
    " " +
    nameValue +
    ":" +
    " " +
    tongTienValue.toLocaleString();
}
// hàm giá tiền điện
function giaTienDiem(sokW) {
  if (sokW <= 50) {
    return 500;
  } else if (sokW > 50 && sokW <= 100) {
    return 650;
  } else if (sokW > 100 && sokW <= 200) {
    return 850;
  } else if (sokW > 200 && sokW <= 350) {
    return 1100;
  } else {
    return 1300;
  }
}
// hàm tính tiền
function tinhTien(kw, giaTien) {
  var tongTien = kw * giaTien;
  return tongTien;
}
